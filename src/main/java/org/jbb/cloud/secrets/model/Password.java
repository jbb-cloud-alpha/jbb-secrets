package org.jbb.cloud.secrets.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.jbb.cloud.swan.autoconfigure.jpa.BaseEntity;

@Entity
@Table
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Password extends BaseEntity {

  @NotNull
  private String memberId;

  @NotBlank
  private String passwordHash;

}

package org.jbb.cloud.secrets.service;

import java.util.Optional;
import org.jbb.cloud.secrets.model.Password;

public interface PasswordService {

  Password savePassword(String memberId, String password);

  void deletePassword(String memberId);

  Optional<Password> getPassword(String memberId);

}

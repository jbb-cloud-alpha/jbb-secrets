package org.jbb.cloud.secrets.service;

import java.util.Optional;
import org.jbb.cloud.secrets.model.Password;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordRepository extends CrudRepository<Password, String> {

  Optional<Password> findByMemberId(String memberId);

  void deleteByMemberId(String memberId);

}

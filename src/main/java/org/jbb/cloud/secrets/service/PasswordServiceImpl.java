package org.jbb.cloud.secrets.service;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.jbb.cloud.secrets.model.Password;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PasswordServiceImpl implements PasswordService {

  private final PasswordRepository passwordRepository;

  @Override
  public Password savePassword(String memberId, String password) {
    Password updatedPassword = passwordRepository.findByMemberId(memberId).orElse(
        Password.builder()
            .memberId(memberId)
            .build()
    );
    updatedPassword.setPasswordHash(password);
    return passwordRepository.save(updatedPassword);
  }

  @Override
  public void deletePassword(String memberId) {
    passwordRepository.deleteByMemberId(memberId);
  }

  @Override
  public Optional<Password> getPassword(String memberId) {
    return passwordRepository.findByMemberId(memberId);
  }
}

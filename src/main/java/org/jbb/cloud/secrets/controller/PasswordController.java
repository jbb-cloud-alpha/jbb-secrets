package org.jbb.cloud.secrets.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.jbb.cloud.secrets.api.v1.PasswordDto;
import org.jbb.cloud.secrets.api.v1.SavePasswordDto;
import org.jbb.cloud.secrets.model.Password;
import org.jbb.cloud.secrets.service.PasswordService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/passwords")
@RequiredArgsConstructor
@Api(value = "/v1/passwords", tags = "Passwords", description = "Manage members passwords")
public class PasswordController {

  private final PasswordService passwordService;

  @ApiOperation("Returns password for member")
  @RequestMapping(method = RequestMethod.GET)
  public PasswordDto getPassword(@RequestParam("memberId") String memberId) {
    return serialize(
        passwordService.getPassword(memberId).orElseThrow(() -> new IllegalArgumentException()));
  }

  @ApiOperation("Creates member password")
  @RequestMapping(method = RequestMethod.POST)
  public PasswordDto savePassword(@RequestBody @Valid SavePasswordDto savePasswordDto) {
    return serialize(
        passwordService.savePassword(savePasswordDto.getMemberId(), savePasswordDto.getPassword()));
  }

  @ApiOperation("Removes member password")
  @RequestMapping(method = RequestMethod.DELETE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteMember(@RequestParam("memberId") String memberId) {
    passwordService.deletePassword(memberId);
  }

  private PasswordDto serialize(Password password) {
    return PasswordDto.builder()
        .memberId(password.getMemberId())
        .passwordHash(password.getPasswordHash())
        .build();
  }

}
